
all:
	gcloud --project gitlab-infra-automation beta functions deploy register --env-vars-file .env.yaml --runtime nodejs8 --trigger-http
	gcloud --project gitlab-infra-automation beta functions deploy incoming --env-vars-file .env.yaml --runtime nodejs8 --trigger-http
	curl --fail https://us-central1-gitlab-infra-automation.cloudfunctions.net/register --data '{"token": "${shell grep REGISTER_TOKEN .env.yaml |cut -d\" -f2}"}' -H "Content-Type:application/json"
